package assignment5;
public interface IShape3D{
    double getVolume();
    double getSurfaceArea();
}